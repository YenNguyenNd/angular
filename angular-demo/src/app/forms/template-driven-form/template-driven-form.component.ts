import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-template-driven-form',
  templateUrl: './template-driven-form.component.html',
  styleUrls: ['./template-driven-form.component.scss']
})
export class TemplateDrivenFormComponent implements OnInit {
  tea = {
    "teaName": "Jasmine tea",
    "wholesalePrice": "20$",
    "retailPrice": "30$",
    "origin": "Ha Giang",
    "description": "Is a tea that combines tea leaves and jasmine"
  }

  constructor() { }

  ngOnInit() {
  }

  onSubmit(form) {
    console.log(this.tea);
    throw Error('something go wrong');
  }

}
