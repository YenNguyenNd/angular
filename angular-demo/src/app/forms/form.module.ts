import { NgModule } from '@angular/core';
import { TemplateDrivenFormComponent } from './template-driven-form/template-driven-form.component';
import { FormRoutingModule } from './form.routing';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [FormRoutingModule, FormsModule, CommonModule],
  declarations: [TemplateDrivenFormComponent],
  providers: []
})
export class FormModule {}
